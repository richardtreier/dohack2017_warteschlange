from swagger_server.services import queue_service

def get():
    """
    Get all tickets
    

    :rtype: List[Ticket]
    """

    return queue_service.get_all()


def next():
    """
    
    Last ticket was used

    :rtype: None
    """
    queue_service.next()


def top():
    """

    Top ticket

    :rtype: None
    """
    return queue_service.top()


def top_noshow():
    """
    
    Newest ticket was not used

    :rtype: None
    """
    queue_service.no_show()
