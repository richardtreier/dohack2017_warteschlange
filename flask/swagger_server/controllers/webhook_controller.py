from swagger_server import config
from swagger_server.services import twilio_message_service, twilio_api_service
from flask import request
import urllib
import urllib.parse

def incoming_twilio_sms(To, From, Body, MessagingServiceSid=None, MediaUrl=None, StatusCallback=None,
                        ApplicationSid=None, MaxPrice=None, ProvideFeedback=None):
    """

    Add a new ticket to queue
    :param To: The destination phone number
    :type To: str
    :param From: A Twilio phone number (in E.164 format) or alphanumeric sender ID enabled for the type of message you wish to send
    :type From: str
    :param Body: The text of the message you want to send, limited to 1600 characters
    :type Body: str
    :param MessagingServiceSid: The 34 character unique id of the Messaging Service you want to associate with this Message
    :type MessagingServiceSid: str
    :param MediaUrl: The URL of the media you wish to send out with the message. gif , png and jpeg content is currently supported and will be formatted correctly on the recipient&#39;s device
    :type MediaUrl: str
    :param StatusCallback: A URL that Twilio will POST to each time your message status changes to one of the following: queued, failed, sent, delivered, or undelivered
    :type StatusCallback: str
    :param ApplicationSid: Twilio will POST MessageSid as well as MessageStatus&#x3D;sent or MessageStatus&#x3D;failed to the URL in the MessageStatusCallback property of this Application
    :type ApplicationSid: str
    :param MaxPrice: The total maximum price up to the fourth decimal (0.0001) in US dollars acceptable for the message to be delivered
    :type MaxPrice: float
    :param ProvideFeedback: Set this value to true if you are sending messages that have a trackable user action and you intend to confirm delivery of the message using the Message Feedback API
    :type ProvideFeedback: bool

    :rtype: None
    """

    msg = {'To': To, 'From': From, 'Body': Body, 'MessagingServiceSid': MessagingServiceSid, 'MediaUrl': MediaUrl,
           'StatusCallback': StatusCallback, 'ApplicationSid': ApplicationSid, 'MaxPrice': MaxPrice,
           'ProvideFeedback': ProvideFeedback}

    twilio_message_service.handle_incoming_message(msg["From"], msg["Body"])


def twilio_call_serve_response_xml(text):
    """
    twilio_call_serve_response_xml

    :param text: Text to be set during the phone call
    :type text: str

    :rtype: None
    """

    return twilio_api_service.api_response_xml(text)


def twilio_call_serve_response_xml_link(text):
    return config.API_ROOT + "/webhooks/twilio/response" + "?text=" + urllib.parse.quote(text)

def incoming_test_sms(customer_number, msg):
    if not config.DEBUG_MODE:
        raise Exception("Only enabled in debug mode!")
    twilio_message_service.handle_incoming_message(customer_number, msg)
