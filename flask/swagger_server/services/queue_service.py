from swagger_server.services import twilio_message_service
import swagger_server.models.ticket as _ticket
from swagger_server import config
import datetime
import sys
import math

_tickets = []
_ticket_id_counter = 1
_no_show_counter = dict()
_validation_code = 696

_recorded_durations = [config.RECORDED_DURATION_HISTORY_INITIAL_ESTIMATE_IN_MINUTES]
_recorded_durations_avg = sum(_recorded_durations) / len(_recorded_durations)


def _is_empty():
    global _tickets
    return len(_tickets) == 0


def get_all():
    global _tickets
    return _tickets


def top():
    if _is_empty():
        return None

    global _tickets
    return _tickets[0]
    # return _ticket.Ticket(id=123, name="None", customer_number="None", reminder_minutes=10, reminder_sent=False)


def next():
    global _tickets
    if _is_empty():
        return None

    _timer_done()
    if len(_tickets) > 1:
        _timer_start()
        _tickets[1].reminder_sent = True
        use_call = not _tickets[1].reminder_sent and _tickets[1].reminder_is_call
        twilio_message_service.send_you_are_next_message(_tickets[1].customer_number, is_call=use_call)

    return _tickets.pop(0)


def add(customer_number, notification_minutes, notification_is_call=False):
    rdynow = False
    if _is_empty():
        _timer_start()
        rdynow = True

    ticket = _ticket.Ticket(name=_generate_ticket_name(), customer_number=customer_number,
                            reminder_is_call=notification_is_call, reminder_sent=False,
                            reminder_minutes=notification_minutes)
    people_ahead = len(_tickets)
    estimated_time_remaining = _calculated_avarage_time_per_customer() * people_ahead
    _tickets.append(ticket)

    if rdynow:
        ticket.reminder_sent = True
        twilio_message_service.send_you_are_next_message(customer_number, ticket.reminder_is_call)
    else:
        twilio_message_service.send_enqueue_success_message(customer_number=ticket.customer_number, ticket_id=ticket.name,
                                                        people_ahead=people_ahead,
                                                        estimated_time_remaining=estimated_time_remaining)

def cancel(customer_number):
    if top().customer_number == customer_number:
        _timer_cancel()

    global _tickets
    _tickets = [x for x in _tickets if x.customer_number != customer_number]

    twilio_message_service.send_cancel_success_message(customer_number)


def no_show():
    if _is_empty():
        return

    global _no_show_counter
    number = top().customer_number

    twilio_message_service.send_you_missed_your_appointment(top().customer_number)

    if (number in _no_show_counter):
        _no_show_counter[number] += 1
    else:
        tmp = {number: 0}
        _no_show_counter.update(tmp)

    next()


def _send_due_message(ticket):
    global _tickets
    people_ahead = _tickets.index(ticket)
    estimated_time_remaining = people_ahead * _calculated_avarage_time_per_customer()

    print("Sending due message for ticket %s" % str(ticket))
    twilio_message_service.send_due_message(ticket.customer_number, estimated_time_remaining, people_ahead,
                                            is_call=ticket.reminder_is_call)
    ticket.reminder_sent = True


def is_enqueued(customer_number):
    global _tickets
    return len([x for x in _tickets if x.customer_number == customer_number]) > 0


def is_correct_validation_code(code):
    global _validation_code
    return int(code) == int(_validation_code)


def is_banned(customer_number):
    global _no_show_counter

    return customer_number in _no_show_counter.keys() and _no_show_counter[customer_number] >= 3


def _generate_ticket_name():
    global _ticket_id_counter
    id = "A%05d" % _ticket_id_counter
    _ticket_id_counter += 1

    return id


def _get_due_reminders():
    global _tickets
    average_time_per_customer = _calculated_avarage_time_per_customer()
    return [x for people_ahead, x in enumerate(_tickets) if
            not x.reminder_sent and people_ahead * average_time_per_customer <= x.reminder_minutes]


def _calculated_avarage_time_per_customer():
    return _recorded_durations_avg;


_last_timestamp = None


def _timer_cancel():
    global _last_timestamp
    _last_timestamp = None


def _timer_start():
    global _last_timestamp
    _last_timestamp = datetime.datetime.now()


def _timer_done():
    global _last_timestamp
    global _recorded_durations
    global _recorded_durations_avg

    recorded_duration_in_minutes = (datetime.datetime.now() - _last_timestamp).total_seconds() / 60.0
    _recorded_durations.append(recorded_duration_in_minutes)
    if len(_recorded_durations) > config.RECORDED_DURATION_HISTORY_SIZE:
        _recorded_durations.pop(0)

    _recorded_durations_avg = sum(_recorded_durations) / len(_recorded_durations)

    print("Average duration in minutes: %f (%s)" % (_recorded_durations_avg, repr(_recorded_durations)),
          file=sys.stderr, flush=True)

    _last_timestamp = None


# Polling thread
import time
import threading


def threader():
    while True:
        time.sleep(0.9)

        for ticket in _get_due_reminders():
            _send_due_message(ticket)


t = threading.Thread(target=threader)
t.daemon = True
t.start()
