from swagger_server import config
from twilio.rest import Client
from twilio.twiml.voice_response import VoiceResponse, Say
from swagger_server.controllers.webhook_controller import twilio_call_serve_response_xml_link
import sys

__mock_twilio_communication = config.DEBUG_MODE
if __mock_twilio_communication:
    print("Mocking twilio communication completely", file=sys.stderr, flush=True)

# Client for Outgoing Twilio Api Calls
def create_client():
    return Client(config.TWILIO_API_KEY, config.TWILIO_API_SECRET, config.TWILIO_ACCOUNT_SID)


# Generate a Twilio Response XML File For Text To Speech Automated Phone Calls
def api_response_xml(text):
    global __mock_twilio_communication
    if not __mock_twilio_communication:
        response = VoiceResponse()
        response.say(message=text, voice=config.TWILIO_TEXT_TO_SPEECH_VOICE, language=config.TWILIO_TEXT_TO_SPEECH_LANGUAGE,
                     loop=5)
        return str(response)
    else:
        return "<!DOCTYPE n shit><toallyxmlbro>AINT NOBODY GOT NO TIME FOR THAT</totallyxmlbro>"



# Send SMS to Phone Number
def send_sms(recipient, content):
    global __mock_twilio_communication
    if not __mock_twilio_communication:
        client =  create_client()
        message = client.messages.create(
            to=recipient,
            from_=config.TWILIO_PHONE_NUMBER,
            body=content
        )

        print("Sent message %s to '%s' with SID %s (Status: %s)." % (repr(content), recipient, message.sid, message.status))
    else:
        print("Sent message %s to '%s'." % (repr(content), recipient), file=sys.stderr, flush=True)



# Send Call with Text To Speech Message Repeated To Phone Number
def send_call(recipient, content):
    global __mock_twilio_communication
    if not __mock_twilio_communication:
        print("XML URL: %s" % twilio_call_serve_response_xml_link(content), file=sys.stderr, flush=True)
        client =  create_client()
        call = client.calls.create(to=recipient, from_=config.TWILIO_PHONE_NUMBER,
                                   url=twilio_call_serve_response_xml_link(content))
        print("Sent call with SID '%s' to '%s' with message %s." % (call.sid, recipient, repr(content)), file=sys.stderr, flush=True)
    else:
        print("Sent message %s to '%s'." % (repr(content), recipient), file=sys.stderr, flush=True)