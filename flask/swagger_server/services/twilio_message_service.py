from swagger_server import config
from swagger_server.services import queue_service, twilio_api_service
import re
import sys


# Send SMS Or Call with Text To Speech Message Repeated To Phone Number
def __send_reminder(recipient, content, is_call=False):
    if is_call:
        twilio_api_service.send_call(recipient, content)
    else:
        twilio_api_service.send_sms(recipient, content)


# Logic for handling an incoming SMS message to our service <3
def handle_incoming_message(from_, message):
    message = str(message).strip().lower()

    print("From '%s' got %s: " % (from_, repr(message)), end="", file=sys.stderr, flush=True)

    if re.fullmatch(pattern="([0-9]{3})([0-9]*)([a]?)", string=message, flags=re.I) != None:  #
        is_call = False
        reminder_minutes = config.REMINDER_DEFAULT_MINUTES
        code = int(message[0:3])

        if message.endswith("a"):
            message = message[:-1]
            is_call = True
            print("It's a call!", end="", file=sys.stderr, flush=True)

        reminder_minutes_str = message[3:]
        if len(reminder_minutes_str) > 0:
            reminder_minutes = int(reminder_minutes_str)

        if queue_service.is_banned(from_):
            print("Banned.", file=sys.stderr, flush=True)
            send_you_are_banned_message(from_)
            return

        if not queue_service.is_correct_validation_code(code):
            print(code, "Wrong Code.", file=sys.stderr, flush=True)
            send_invalid_code_message(from_)
            return

        if queue_service.is_enqueued(from_):
            print("Already enqueued.", file=sys.stderr, flush=True)
            send_you_are_already_enqueued_message(from_)
            return

        print(reminder_minutes, is_call, "ADDED", file=sys.stderr, flush=True)
        queue_service.add(customer_number=from_, notification_is_call=is_call,
                          notification_minutes=reminder_minutes)
        return

    if message == config.STOP_WORD:
        if not queue_service.is_enqueued(from_):
            print("Not enqueued yet.", file=sys.stderr, flush=True)
            send_not_yet_enqueued_message(from_)
            return

        print("CANCELLED", file=sys.stderr, flush=True)
        queue_service.cancel(from_)
        return

    print("Sent HELP message.", file=sys.stderr, flush=True)
    send_help_message(from_)


# Message / Call: Reminder
def send_due_message(customer_number, estimated_time_remaining, people_ahead, is_call=False):
    msg = "Ihr Termin ist voraussichtlich in %d Minuten bereit. Vor Ihnen befinden sich %d Personen in der Warteschlange." % (
        estimated_time_remaining, people_ahead)

    __send_reminder(customer_number, msg, is_call)


# Message: Successfully Enqueued
def send_enqueue_success_message(customer_number, ticket_id,
                                 people_ahead,
                                 estimated_time_remaining):
    twilio_api_service.send_sms(customer_number,
                                'Ihre Wartenummer: %s. Vor Ihnen befinden sich %d Personen in der Schlange. Die erwartete Wartezeit beträgt %d Minuten.' % (
                                    ticket_id, people_ahead, estimated_time_remaining))


# Message: Successfully Cancelled
def send_cancel_success_message(customer_number):
    twilio_api_service.send_sms(customer_number, "Sie haben ihren Platz in der Warteschlange erfolgreich aufgegeben.")


# Message: Banned
def send_you_are_banned_message(customer_number):
    twilio_api_service.send_sms(customer_number,
                                "Sie sind momentan gesperrt, da sie zu zu vielen Terminen nicht erschienen sind. Versuchen Sie es morgen wieder.")


# Message: Already Enqueued
def send_you_are_already_enqueued_message(customer_number):
    twilio_api_service.send_sms(customer_number,
                                "Sie sind bereits in der Warteschlange.")


# Message: Not yet Enqueued
def send_not_yet_enqueued_message(customer_number):
    twilio_api_service.send_sms(customer_number,
                                "Sie waren nicht in der Warteschlange eingetragen.")


# Message: Invalid Code
def send_invalid_code_message(customer_number):
    twilio_api_service.send_sms(customer_number,
                                "Sie haben einen falschen Verifizierungscode angegeben.,")


def send_you_missed_your_appointment(customer_number):
    twilio_api_service.send_sms(customer_number,
                                "Sie haben Ihren Termin verpasst.")

def send_you_are_next_message(customer_number, is_call=False):
    __send_reminder(customer_number,
                                "Sie sind nun an der Reihe. Viel Spaß!", is_call=is_call)


# Message: Help / Info
def send_help_message(customer_number):
    twilio_api_service.send_sms(customer_number,
                                "Ihre letzte Nachricht konnte nicht interpretiert werden.")
    twilio_api_service.send_sms(customer_number,
                                "Senden Sie \"ABC30\" bei Verifizierungscode ABC, um 30 Minuten vor ihrem Termin eine Benachrichtigung per SMS zu bekommen.")
    twilio_api_service.send_sms(customer_number,
                                "Senden Sie \"ABC30A\" bei Verifizierungscode ABC, um 30 Minuten vor ihrem Termin eine Benachrichtigung per Telefon zu bekommen.")
    twilio_api_service.send_sms(customer_number,
                                "Senden Sie \"" + config.STOP_WORD + "\" um ihren Platz in der Warteschlange zu stornieren.")
    twilio_api_service.send_sms(customer_number,
                                "Nichterscheinen bei einem Termin kann zur Sperrung führen.")
