# coding: utf-8

from __future__ import absolute_import

from swagger_server.models.ticket import Ticket
from . import BaseTestCase
from six import BytesIO
from flask import json


class TestQueueController(BaseTestCase):
    """ QueueController integration test stubs """

    def test_add_ticket_from_sms(self):
        """
        Test case for add_ticket_from_sms

        
        """
        data = dict(To='To_example',
                    From='From_example',
                    MessagingServiceSid='MessagingServiceSid_example',
                    Body='Body_example',
                    MediaUrl='MediaUrl_example',
                    StatusCallback='StatusCallback_example',
                    ApplicationSid='ApplicationSid_example',
                    MaxPrice=1.2,
                    ProvideFeedback=false)
        response = self.client.open('/v1/webhooks/twilio/sms',
                                    method='POST',
                                    data=data,
                                    content_type='application/x-www-form-urlencoded')
        self.assert200(response, "Response body is : " + response.data.decode('utf-8'))

    def test_get_all_tickets(self):
        """
        Test case for get_all_tickets

        Get all tickets
        """
        response = self.client.open('/v1/queue',
                                    method='GET')
        self.assert200(response, "Response body is : " + response.data.decode('utf-8'))

    def test_next_ticket(self):
        """
        Test case for next_ticket

        
        """
        response = self.client.open('/v1/queue/next',
                                    method='POST',
                                    content_type='application/json')
        self.assert200(response, "Response body is : " + response.data.decode('utf-8'))

    def test_top_noshow(self):
        """
        Test case for top_noshow

        
        """
        response = self.client.open('/v1/queue/top/noshow',
                                    method='POST',
                                    content_type='application/json')
        self.assert200(response, "Response body is : " + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
