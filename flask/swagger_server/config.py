#Config files
CONFIG_FILES_DIR = "../.."
CONFIG_FILES = ["config.properties", "config-secret.properties"]

#Load Config
import os
from swagger_server.config_utils import read_properties_file
config_values_list = []
for config_file_name in CONFIG_FILES:
    config_file_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), CONFIG_FILES_DIR, config_file_name)
    if not os.path.exists(config_file_path):
        raise Exception("Need config file %s" % os.path.abspath(config_file_path))

    config_values_list.extend(read_properties_file(config_file_path))

CONFIG = dict(config_values_list)

#Get Property Function
def get_property(key, required=False, default=None):
    if key not in CONFIG.keys():
        if required:
            raise Exception("Required property %s is missing in property files %s" % (key, str(CONFIG_FILES)))

        return default

    return CONFIG[key]

#Properties
TWILIO_ACCOUNT_SID = get_property("twilio.account_sid", required=True)
TWILIO_API_KEY = get_property("twilio.api_key", required=True)
TWILIO_API_SECRET = get_property("twilio.api_secret", required=True)
TWILIO_PHONE_NUMBER = get_property("twilio.phoneNumber", required=True)
TWILIO_TEXT_TO_SPEECH_LANGUAGE = get_property("twilio.text_to_speech.language", required=True)
TWILIO_TEXT_TO_SPEECH_VOICE = get_property("twilio.text_to_speech.voice", required=True)

REMINDER_DEFAULT_MINUTES = int(get_property("reminder.default", default="5"))
STOP_WORD = get_property("keyword.stop", default="rqqueue")

RECORDED_DURATION_HISTORY_SIZE = int(get_property("history.size", default="5"))
RECORDED_DURATION_HISTORY_INITIAL_ESTIMATE_IN_MINUTES = int(get_property("history.initialEstimate", default="5"))


DEBUG_MODE = get_property("debug.mode", default="false") == "true"

HOST = "http://ec2-52-212-159-55.eu-west-1.compute.amazonaws.com:8080"
if DEBUG_MODE:
    HOST = "http://localhost:8080"

API_ROOT = HOST + "/api/v1"