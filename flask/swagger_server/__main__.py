#!/usr/bin/env python3

import connexion
from .encoder import JSONEncoder
from flask_cors import CORS, cross_origin
from . import config


if __name__ == '__main__':
    app = connexion.App(__name__, specification_dir='./swagger/')
    CORS(app.app)
    app.app.json_encoder = JSONEncoder
    app.add_api('swagger.yaml', arguments={'title': 'Queue API'})

    app.run(port=8080)
