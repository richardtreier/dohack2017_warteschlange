def read_file(path):
    with open(path) as f:
        return str(f.read())

def parse_property_file_line(line):
    line = str(line).strip()
    if line.startswith("#"):
        return None

    parts =list(map(lambda x: x.strip(), line.split("=", maxsplit=1)))

    if len(parts) < 2:
        return None

    return parts

def read_properties_file(path):
    return list(filter(lambda x: x != None, map(lambda x: parse_property_file_line(x), read_file(path).splitlines())))
