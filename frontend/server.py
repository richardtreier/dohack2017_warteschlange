from flask import Flask, request, send_from_directory
import os

app = Flask(__name__, static_url_path='')


@app.route('/')
def root():
	return send_from_directory('', 'index.html')

@app.route('/scripts/<path:path>')
def send_scripts(path):
    return send_from_directory('scripts', path)

@app.route('/stylesheets/<path:path>')
def send_stylesheet(path):
    return send_from_directory('stylesheets', path)

@app.route('/sites/<path:path>')
def send_sites(path):
    return send_from_directory('sites', path)

@app.route('/images/<path:path>')
def send_images(path):
    return send_from_directory('images', path)

@app.errorhandler(404)
def page_not_found(error):
    return send_from_directory('sites', '404.html')

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8081)
	