// Functions are called when DOM is loaded.
$(function(){
	data();
	window.setInterval(function(){
		data();
	}, 5000);
	next();
	no_show();
	time();
});

var API_ENDPOINT = "http://ec2-52-212-159-55.eu-west-1.compute.amazonaws.com:8080/api/v1/";

function data(){
	var count = 0;
	var number = 1;
	$.getJSON(API_ENDPOINT + "queue", function(result){
		if(result.length > 0){
			$("#name").text(result[0].name);
			$("#queue").text(result.length);
			$("#lvl1").show();
			if(result.length > 1){
				$("#name_2").text(result[1].name);
				$("#lvl2").show();
				
				if(result.length > 2){
					$("#name_3").text(result[2].name);
					$("#lvl3").show();
				} else {
					$("#lvl3").hide();
				}
			}	else {
				$("#lvl2").hide();
			}
			$("#next").prop("disabled", false);
			$("#no_show").prop("disabled", false);
		}
		else{
			$("#name").text("-");
			$("#queue").text("0");
			$("#next").prop("disabled", true);
			$("#no_show").prop("disabled", true); 
			$("#lvl1").hide();
		}
	});
}

function do_next(showed){
	if(showed){
		var count = parseInt($("#finished").text());
		$("#finished").text(count + 1);
		$.post(API_ENDPOINT + "queue/next");
	}
	else{
		$.post(API_ENDPOINT + "queue/top/noshow");
	}
	data();
}

function next(){
	$("#next").click(function(){
		do_next(true);
	});
}

function no_show(){
	$("#no_show").click(function(){
		do_next(false);
	});
}

function time(){
	var date = new Date();
	var hours = date.getHours();
	if(hours < 10){
		hours = "0" + hours;
	}
	var minutes = date.getMinutes();
	if(minutes < 10){
		minutes = "0" + minutes;
	}
	var seconds = date.getSeconds();
	if(seconds < 10){
		seconds = "0" + seconds;
	}
	$("#time").text(hours + ":" + minutes + ":" + seconds);
	setTimeout(function(){
		time();
	}, 1000);
}